
from flask import Flask, render_template, url_for, request, json, redirect
import pymysql
from datetime import datetime
from kafka import KafkaProducer
from kafka import KafkaConsumer
from flask import flash
from flask_login import current_user, login_user
import time
from flaskext.mysql import MySQL
from werkzeug.security import generate_password_hash, check_password_hash
	
from flask import session


mysql = MySQL()
 
app = Flask(__name__)
app.secret_key = 'Ahgxcp1!#G'
# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'bill'
app.config['MYSQL_DATABASE_PASSWORD'] = 'passpass'
app.config['MYSQL_DATABASE_DB'] = 'ACCOUNTS'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)

def check_cart(item_code):

  conn = mysql.connect()
  cursor = conn.cursor()
  sqlstatement = "select * from cart WHERE item_code = %s"
  cursor.execute(sqlstatement, item_code)
  
  data = cursor.fetchall()
  data_list = []
  
  #Siff through table to append qty to data_list
  for i in data:
    data_list.append(i[3])

  count = 0
  
  #Accumulate the data_list to find total count of product
  for i in data_list:
    count += i
  print(count)
  if count >= 100:
    return True
  else: 
    return False
    
check_cart("s1002")
x = check_cart("s1002")
print(x)
