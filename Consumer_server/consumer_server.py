from flask import Flask, render_template, url_for, request, json, redirect, jsonify
from flask import session
from flaskext.mysql import MySQL
import kafka
from kafka import KafkaConsumer
from datetime import datetime
from json import loads
import time
import json
import sys
import pymysql
from flask import send_file
from flask import flash
import random
from werkzeug.security import generate_password_hash, check_password_hash
import requests as req
#mysql -u bill -p
#passpass

#create database ksql

#REFER TO MYSQLREADME.TXT FOR MORE INFO!

#Connect to mysql
mysql = MySQL()
                     
#Connect to flask                         
app = Flask(__name__)
#secret key
app.secret_key = 'This_is_consumer_server_key'
# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'bill'
app.config['MYSQL_DATABASE_PASSWORD'] = 'passpass'
app.config['MYSQL_DATABASE_DB'] = 'ksql'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)

#To get current time in string
def timenow():
  now = datetime.now()
  str_now = str(now)
  str_now = str_now[:-6]
  return str_now

@app.route('/check_server')
def check_server():
  try:
    resp = req.request(method='GET', url="http://127.0.0.1:5000/")
    if (resp.status_code) == 200:
      e_server = "Online"
      session['e_server'] = "Online"
    else:
      e_server ="Offline"
      session['e_server'] = "Offline"
    print(e_server)
  except:
    #flash("Unable to connect to e-commerce server!")
    
    e_server ="Offline"
    session['e_server'] = "Offline"
           
    return jsonify(e_server=e_server)
    #return redirect('dashboard')
  return jsonify(e_server=e_server)
  
  
@app.route('/check_server_norm')
def check_server_norm():
  try:
    resp = req.request(method='GET', url="http://127.0.0.1:5000/")
    if (resp.status_code) == 200:
      server_status = "Online"
      session['server_status'] = "Online"
    else:
      server_status ="Offline"
      session['server_status'] = "Offline"
    
  except:
    #flash("Unable to connect to e-commerce server!")
    server_status ="Offline"
    session['server_status'] = "Offline"
    error = "E-Commerce server is offline!"
    return server_status
    #return redirect('dashboard')
  return server_status
#home page
@app.route('/', methods=['GET'])
def index():
  if session.get('user'):
    
    topic = "server"
    #Initialize Kafka consumer
    consumer = KafkaConsumer(topic, bootstrap_servers=['localhost:9092'],auto_offset_reset='latest',consumer_timeout_ms=1000, enable_auto_commit=True, value_deserializer=lambda m: json.loads(m.decode('utf-8')))

    topics = consumer.topics()
    #Check kafka consumer status
    print(topics)
    if not topics:
      server_status ="Not running"
    else:
      server_status = "Running"
     
    #Check e-com server through URL 
    e_status = check_server_norm()


    
    x = 0
    count = count_unread()
    
    return render_template('monitorApp.html', e_status = e_status ,  server_status =server_status, count = count)
  else:
    return redirect('login')


#Total registration function 
@app.route('/total_reg', methods=['GET'])    
def total_reg():
  con = mysql.connect()
  cursor = con.cursor()
  reg = "Registration"
  sqlstatement = "SELECT COUNT(*) from account_service WHERE activity= %s"
  
  cursor.execute(sqlstatement, reg)
  
  #Get total users registered
  _total_user = cursor.fetchone()
  total_user = _total_user[0]
  
  con.close()
  cursor.close()
  #con.commit()
  return jsonify(total_user=total_user)


@app.route('/get_topic', methods=['POST'])
def get_topic():
 #Topic name = request from html
  topic = request.form['topic_name']
  session['topic'] = topic

  return redirect('/index/monitorlog')

@app.route('/index/monitorlog/sort_latest')
def sort_latest():
  con = mysql.connect()
  cursor = con.cursor()
  
 #Example of sql statement
 #select *  from order_service where activity = 'Ordered' order by date DESC, time DESC;
# select * from cart_service order by date DESC, time DESC;
  topic = session.get('topic')
  svc = session.get('svc')
  activity = session.get('activity')
  #Select statement SQL
  sql ='select * FROM ' + svc + ' where activity = ' + '"'+ activity +'"' + ' order by date DESC, time DESC'
  #print(st)
  cursor.execute(sql)
  data = cursor.fetchall()
  
  data_dict = identifyTopic(data)
  
  
  con.close()
  cursor.close()
  #Return the data_dict to display it on HTML
  if topic == "user_logon":
    return render_template("monitorlogin.html", data_dict = data_dict) 

  if topic == "user_logout":
    return render_template("monitorlogout.html", data_dict = data_dict) 
    
  if topic == "user_creation":
    return render_template("monitorsignup.html", data_dict = data_dict) 
    
  if topic == "cart_service":
    return render_template("monitorcart.html", data_dict = data_dict) 

  if topic == "search_service":
    return render_template("monitorsearch.html", data_dict = data_dict) 

  if topic == "order_service":
    return render_template("monitorOrder.html", data_dict = data_dict)
  
@app.route('/index/monitorlog/sort_old')
def sort_old():
  con = mysql.connect()
  cursor = con.cursor()
 #select *  from order_service where activity = 'Ordered' order by date ASC;
  topic = session.get('topic')
  svc = session.get('svc')
  activity = session.get('activity')
  #Select statement SQL
  sql ='select * FROM ' + svc + ' where activity = ' + '"'+ activity +'"' +' order by date ASC'
  #print(st)
  cursor.execute(sql)
  data = cursor.fetchall()
  
  #call identify topic function 
  data_dict = identifyTopic(data)
  #Return the data_dict to display it on HTML
  con.close()
  cursor.close()
  if topic == "user_logon":
    return render_template("monitorlogin.html", data_dict = data_dict) 

  if topic == "user_logout":
    return render_template("monitorlogout.html", data_dict = data_dict) 
    
  if topic == "user_creation":
    return render_template("monitorsignup.html", data_dict = data_dict) 
    
  if topic == "cart_service":
    return render_template("monitorcart.html", data_dict = data_dict) 

  if topic == "search_service":
    return render_template("monitorsearch.html", data_dict = data_dict) 

  if topic == "order_service":
    return render_template("monitorOrder.html", data_dict = data_dict)
  
#Display the logs of selected service 
@app.route('/index/monitorlog')
def monitorlog():
 
  topic = session.get('topic')  
  try:    
    #Initialize mySQL
    con = mysql.connect()
    cursor = con.cursor() 
    
    #assign variables depending on which topic user has selected
    if topic == "user_logon":
      svc = "account_service"
      activity = "Login"
      
    if topic == "user_logout":
      svc = "account_service"
      activity = "Logout"
      
    if topic == "user_creation":
      svc = "account_service"
      activity = "Registration"
      
    if topic == "cart_service":
      svc = "cart_service"
      activity = "Added to cart"
      
    if topic == "search_service":
      svc = "search_service"
      activity = "Searched" 
    if topic == "order_service":
      svc = "order_service"
      activity = "Ordered" 

    #save svc and activity data in session
    session['svc'] = svc
    session['activity'] = activity
    
    #Select statement SQL
    st ='select * FROM ' + svc + ' where activity = ' + '"'+ activity +'"'
    #print(st)
    cursor.execute(st)
  
    record = cursor.fetchall()
    
    #Pass records to identify topic function
    data_dict = identifyTopic(record)
    
    cursor.close()
    con.close()
    #notification = 0
    notification_count = count_unread()
    
    #Return the data_dict to display it on HTML
    if topic == "user_logon":
      return render_template("monitorlogin.html", data_dict = data_dict, count = notification_count ) 

    if topic == "user_logout":
      return render_template("monitorlogout.html", data_dict = data_dict, count = notification_count) 
      
    if topic == "user_creation":
      return render_template("monitorsignup.html", data_dict = data_dict, count = notification_count) 
      
    if topic == "cart_service":
      return render_template("monitorcart.html", data_dict = data_dict, count = notification_count) 

    if topic == "search_service":
      return render_template("monitorsearch.html", data_dict = data_dict, count = notification_count) 

    if topic == "order_service":
      return render_template("monitorOrder.html", data_dict = data_dict, count = notification_count)
    
  except:
    flash("Data cannot be empty!")
    return redirect('/')

#Cart rate function
#To display how many times each products has been added to cart
@app.route('/cart_rate')    
def cart_rate():
  topic = session.get('topic')
  con = mysql.connect()
  cursor = con.cursor() 
  val = "Added to cart"
  
  #Sql statement
  sqlstatement = "SELECT * from cart_service WHERE activity= %s"
  
  cursor.execute(sqlstatement, val)
  data = cursor.fetchall()
  print(data)
  
  cursor.close()
  con.close()
  
  #Initialize 2 list and 1 dictionary 
  data_list=[]
  data_dict={}
  cart_list = []
  
  #sort data using temp dictionary and append to list   
  for i in data:   
    temp_dict = {"cartitem": i[3]}
    data_list.append(temp_dict)
  
  #sort data from data_list into data_dict   
  for i in data_list:
    #print(i)
    j = i["cartitem"]
    #Check if i exist in data_dict 
    if j not in data_dict:
      #initiate value for dictionary item : 0
      data_dict[j] = 0
    #item : 0 + 1  
    data_dict[j] = data_dict[j] + 1
  
  
  #Write to file (output to file)   
  f = open('%s.txt' % topic, "w")  
  for i in data_dict:
  #Temp dict to sort items in data_dicti
    temp_dict = {"Item_name"  : i,
                 "Cart_rate" :data_dict.get(i)}
    
    #write to file
    f.write("Item_name: ")
    f.write(i)
    f.write("\nCart_rate: ")
    
    f.write(str(data_dict.get(i)))
    f.write("\n")
    
    #Append the temp dict to cart list  
    cart_list.append(temp_dict)
  
  print(cart_list)
  f.close()
      
  return render_template('cartrate.html', cart_list=cart_list)

#Download button function    
@app.route('/download')
def downloadFile():

  topic = ""
  #get topic input
  if session.get('topic'):
    topic = session.get('topic')
  else: 
    topic = "output"
  
  #string variable
  path = "%s.txt" %topic
  f = path
  return send_file(f, as_attachment=True)
  
#total user function  
@app.route('/user_online', methods=['GET'])    
def user_online():
  con = mysql.connect()
  cursor = con.cursor()
  login = "Login"
  logout = "Logout"
  #Get login number
  sqlstatement = "SELECT COUNT(*) from login_count WHERE activity= %s"
  
  cursor.execute(sqlstatement, login)
  _logins = cursor.fetchone()
  logins = _logins[0]
  
  #Get logout number
  cursor.execute(sqlstatement, logout)
  _logouts = cursor.fetchone()
  logouts = _logouts[0]
  
  online = (int(logins) - int(logouts))
  
  print(online)
  
  
  return jsonify(online = online)
@app.route('/topsearch', methods=['GET'])    
def topsearch():
  con = mysql.connect()
  cursor = con.cursor()
  val = "search_service"
  sqlstatement = "SELECT * from search_service WHERE topic= %s"
  cursor.execute(sqlstatement, val)
  data = cursor.fetchall()
  #print(data)
  try:
    if len(data) != 0:
      search_dict = []
      temp_dict = {}
      #calculate each of the key words
      for i in data:
        #print(i)
        
        if i[3] not in temp_dict:
          temp_dict[i[3]] = 0
      
        temp_dict[i[3]] = temp_dict[i[3]] + 1
        #search_dict.append(temp_dict)
    
      #print(search_dict)
      print(temp_dict)
     
      _max_key = max(temp_dict, key=temp_dict.get)
      max_key = _max_key
    
      all_values = temp_dict.values()
      max_value = max(all_values)
      con.commit()
      con.close()
      cursor.close()
    return jsonify(max_key = max_key, max_value=max_value)
  except:
    return "Error: There is no search data" 



@app.route('/index/monitorlog/filter', methods=['POST'])
def filter():
  userinput2 = request.form['selectfilter']
  userinput = request.form['filter']
  #userinput = request.form['filter']  
  print("Test:", userinput)
  print("Test2:", userinput2)
  if len(userinput2) != 0 and userinput2 == 'User_email':
    con = mysql.connect()
    cursor = con.cursor()
    #select = request.form['selectfilter'] 
    topic = session.get('topic')
    #data = session.get('data')

    activity = session.get('activity')
    svc = session.get('svc')

    sql = "select * from " + svc + " where activity = %s AND email = %s"

    val = (activity, userinput)
    cursor.execute(sql, val)
    
    record = cursor.fetchall() 
    print("TEST:")
    print(record)
    
    
    cursor.close()
    con.close()
    data_dict = []
    
    data_dict = identifyTopic(record)
    if topic == "user_logon":
      return render_template("monitorlogin.html", data_dict = data_dict) 

    if topic == "user_logout":
      return render_template("monitorlogout.html", data_dict = data_dict) 
      
    if topic == "user_creation":
      return render_template("monitorsignup.html", data_dict = data_dict) 
      
    if topic == "cart_service":
      return render_template("monitorcart.html", data_dict = data_dict) 
 
    if topic == "search_service":
      return render_template("monitorsearch.html", data_dict = data_dict) 

    if topic == "order_service":
      return render_template("monitorOrder.html", data_dict = data_dict) 
  else: 
    error= "Please enter a correct data"
    return render_template("error.html", error=error)

@app.route('/index/monitorlog/filter_monthly', methods=['POST'])
def filter_monthly():
 
  userinput = request.form['selectfilter']
  if userinput == "Choose here":
    flash("Please select a month")
    return redirect('/index/monitorlog')
  
  if len(userinput) != 0:
    
    #Start SQL connecter and cursor 
    con = mysql.connect()
    cursor = con.cursor()
    
    #Get info ussing session 
    topic = session.get('topic')
    activity = session.get('activity')
    svc = session.get('svc')
    
    #Convert date into sql recognisable date
    if userinput == "Jan":
      dateinput = '2022-01-01'
      dateinput2 = '2022-01-31'
    if userinput == "Feb":
      dateinput = '2022-02-01'
      dateinput2 = '2022-02-31'
    if userinput == "March":
      dateinput = '2022-03-01'
      dateinput2 = '2022-03-31'
    if userinput == "April":
      dateinput = '2022-04-01'
      dateinput2 = '2022-04-31'    
    if userinput == "May":
      dateinput = '2022-05-01'
      dateinput2 = '2022-05-31'
    if userinput == "June":
      dateinput = '2022-06-01'
      dateinput2 = '2022-06-31'
    if userinput == "July":
      dateinput = '2022-07-01'
      dateinput2 = '2022-07-31'
    if userinput == "Aug":
      dateinput = '2022-08-01'
      dateinput2 = '2022-08-31'  
    if userinput == "Sep":
      dateinput = '2022-09-01'
      dateinput2 = '2022-09-31'
    if userinput == "Oct":
      dateinput = '2022-10-01'
      dateinput2 = '2022-10-31'
    if userinput == "Nov":
      dateinput = '2022-11-01'
      dateinput2 = '2022-11-31'
    if userinput == "Dec":
      dateinput = '2022-12-01'
      dateinput2 = '2022-12-31'        
    
    #SQL statement with string variables        
    sql = "select * from " + svc + " where activity = " + "'" + activity + "'" + " AND date BETWEEN %s AND %s"

    #Sample sql statement 
    #select * from account_service where activity = "Login" AND date BETWEEN '2022-01-01' AND '2022-01-31';
    #select * from account_service where activity = "Login" AND date BETWEEN '2022-02-01' AND '2022-02-31';
    
    #assign variables to %s 
    val = (dateinput, dateinput2)
    
    #Execute sql statement with cursor
    cursor.execute(sql, val)
    
    #Fetch all records
    record = cursor.fetchall() 

    print(record)
    
    cursor.close()
    con.close()
    data_dict = []
    
    #Call identify topic to digest the SQL data records
    data_dict = identifyTopic(record)
    
    #Render the respective HTML pages depending on the topic selected
    if topic == "user_logon":
      return render_template("monitorlogin.html", data_dict = data_dict) 

    if topic == "user_logout":
      return render_template("monitorlogout.html", data_dict = data_dict) 

    if topic == "user_creation":
      return render_template("monitorsignup.html", data_dict = data_dict) 
  
    if topic == "cart_service":
      return render_template("monitorcart.html", data_dict = data_dict) 

    if topic == "search_service":
      return render_template("monitorsearch.html", data_dict = data_dict) 

    if topic == "order_service":
      return render_template("monitorOrder.html", data_dict = data_dict) 
  else: 
    flash("Please enter a correct data")
    return render_template("displaytopic.html")  
    
    
@app.route('/identifyTopic', methods=['POST'])
def identifyTopic(record):
  
  topic = session.get('topic')
  data_dict = []
  f = open('%s.txt' % topic, "w")
  if topic =="cart_service":
    for i in record:
    #Temp dict for easy sorting of data from SQL
      temp_dict = {
        "ID" : i[0],
        "Email" : i[1],
        "Activity": i[2],
        "item" : i[3],
        "qty" : i[4],
        "Message" : i[5],
        "Date" : i[6],
        "Time": i[7],
        "Topic": i[8]}
      #Output to file 
      f.write('ID: ')
      f.write(str(i[0]))
      f.write(', Topic: ')
      f.write(str(i[7]))
      f.write("\nEmail: " )
      f.write(i[1])
      f.write('\nEvent Occured: ')
      f.write(str(i[2]))
      f.write('\nDate: ')
      f.write( str(i[6]))
      f.write('\nitem_name: ')
      f.write(str(i[3]))
      f.write(', Quantity: ')
      f.write(str(i[4]))
      #f.write("\nfull Message: ")
      #f.write(i[5])
      f.write('\n')
      f.write('\n')
      #temp dict gets appended to data_dict
      data_dict.append(temp_dict)

  if topic =="search_service":
    for i in record:
    #Temp dict for easy sorting of data from SQL
      temp_dict = {
        "ID" : i[0],
        "Email" : i[1],
        "Activity": i[2],
        "keyword" : i[3],
        "Date" : i[5],
        "Time": i[6],
        "Topic": i[7]}
      #Output to file 
      f.write('ID: ')
      f.write(str(i[0]))
      f.write(', Topic: ')
      f.write(str(i[5]))
      f.write("\nEmail: " )
      f.write(i[1])
      f.write('Date: ')
      f.write( str(i[5]))
      f.write('Time: ')
      f.write( str(i[6]))
      f.write('\nEvent Occured: ')
      f.write(str(i[2]))
      f.write("\nkeyword: ")
      f.write(i[3])
      f.write('\n')
      f.write('\n')
      #temp dict gets appended to data_dict
      data_dict.append(temp_dict)
  if topic == "user_logon" or topic == "user_logout" or topic ==  "user_creation":
    for i in record:
    #Temp dict for easy sorting of data from SQL
      temp_dict = {
        "ID" : i[0],
        "Email" : i[1],
        "Activity": i[2],
        "Message" : i[3],
        "Date" : i[4],
        "Time" : i[5],
        "Topic": i[6]}
      #Output to file 
      f.write('ID: ')
      f.write(str(i[0]))
      f.write(', Topic: ')
      f.write(str(i[5]))
      f.write("\nEmail: " )
      f.write(i[1])
      f.write('Date: ')
      f.write( str(i[4]))
      f.write('Time: ')
      f.write( str(i[5]))
      f.write('\nEvent Occured: ')
      f.write(str(i[2]))
      f.write("\nfull Message: ")
      f.write(i[3])
      f.write('\n')
      f.write('\n')
      #temp dict gets appended to data_dict
      data_dict.append(temp_dict)     
  if topic == "order_service":
    for i in record:
    #Temp dict for easy sorting of data from SQL
      temp_dict = {
        "ID" : i[0],
        "Email" : i[1],
        "Activity": i[2],
        "orderID" : i[3],
        "Date" : i[5],
        "Time" : i[6],
        "Topic": i[7],
        "sliceID": i[4][50:64]
        }
      #Output to file 
      f.write("Email: " )
      f.write(i[1])
      f.write(', Date: ')
      f.write( str(i[5]))
      f.write( str(i[6]))
      f.write(', Event Occured: ')
      f.write(str(i[2]))
      f.write(', Order ID: ')
      f.write(str(i[3]))




      f.write('\n')
      f.write('\n')
      #temp dict gets appended to data_dict
      data_dict.append(temp_dict)      
  return (data_dict)     

#Counter
counter = 0
def uniqueid(self):
  self.counter+= 1

#Check unread notification 
@app.route('/notification_unread')
def count_unread():
  
  con = mysql.connect()
  cursor = con.cursor()
  
  cursor.execute("SELECT COUNT(*) FROM notification where checked_status = 0")
  data = cursor.fetchone()
  y = data[0]
  #print("unread message: ", y)
  return jsonify(y=y)
  
  
#Notification
@app.route('/notification')
def notification():

  con = mysql.connect()
  cursor = con.cursor()
  
  cursor.execute("SELECT * FROM notification where checked_status = 0")
  data = cursor.fetchall()
  data_dict = []
  for i in data:
      temp_dict = {
              
              'Email':i[1],
              'Activity': i[2],
              'Message': i[3],
              'Date': i[4],
              'Time': i[5]}
      data_dict.append(temp_dict)
      
  cursor.close()
  con.commit()
  con.close()
   
  return render_template('notification_new.html', data_dict = data_dict)


#Important notification page
@app.route('/important_notification')
def important_notification():

  con = mysql.connect()
  cursor = con.cursor()
  
  cursor.execute("select * from important")
  data = cursor.fetchall()
  data_dict = []
  for i in data:
      temp_dict = {
              'ID' :i[0],
              'Message': i[1],
              'Date': i[2],
              'Time': i[3]}
      data_dict.append(temp_dict)
      
  cursor.close()
  con.commit()
  con.close()
  return render_template('important_notification.html', data_dict = data_dict)
  
  
@app.route('/delete_notification', methods =['POST'])
def delete_notification():  
  con = mysql.connect()
  cursor = con.cursor()
  ID = request.form['ID']

  
  val = ID
  sql = "delete from important where id = %s"
  cursor.execute(sql, val)
  con.commit()
  con.close()
  return redirect('important_notification')

#Flag notification as important
@app.route('/flag_important', methods =['POST'])
def flag_important():
  message = request.form['alert']
  date = request.form['date']
  time = request.form['time']
  
  con = mysql.connect()
  cursor = con.cursor()
  cursor.execute("INSERT INTO important (content, date, time) VALUES (%s, %s, %s)", (message, date, time))
  con.commit()
  con.close()
  cursor.close()
  
  return redirect('notification')
  


#Set all notification to be read
@app.route('/read_all')
def read_all():  
  con = mysql.connect()
  cursor = con.cursor()
  
  #sql = "UPDATE customers SET address = 'Canyon 123' WHERE address = 'Valley 345'"
  cursor.execute("UPDATE notification SET checked_status = 1 where checked_status = 0")
  cursor.close()
  con.commit()
  con.close()
  
  return redirect('notification')
  
@app.route('/history_notification')
def history_notification():    

  #mySQL connection
  con = mysql.connect()
  cursor = con.cursor()  
  #SQL statement
  cursor.execute("SELECT * FROM notification where checked_status = 1")
  cursor.close()
  con.commit()
  con.close()
  
  #Fetch all data from cursor
  data = cursor.fetchall()
  
  #Data dictionary
  data_dict = []
  for i in data:
      temp_dict = {
              'Email':i[1],
              'Activity': i[2],
              'Message': i[3],
              'Date': i[4]}
      data_dict.append(temp_dict)
  return render_template('notification_history.html', data_dict = data_dict)  
  
@app.route('/login')
def loginpage():
  if session.get('user'):
    flash("You have already logged in")
    return redirect('dashboard')
  else:
    return render_template('monitor_loginpage.html')
  
@app.route('/dashboard')
def dashboard():
  if session.get('user'):
    user= session.get('user')
    role=session.get('role')
    server_status = check_server_norm()
    notification = 0
    notification_count = count_unread()
    if role == 'admin':
      view_log = "View logs : True"
      delete_log = "Delete logs : True"
      delete_user = "Delete users: True"
      create_user = "Create users: True"
    if role == "user":
      view_log = "View logs : True"
      delete_log = "Delete logs : False"
      delete_user = "Delete users: False"
      create_user = "Create users: False"
    
    
    return render_template('dashboard_new.html', view_log = view_log,delete_log=delete_log,delete_user=delete_user,create_user=create_user, server_status=server_status, user= user, role = role, count= notification_count)
  else:
    flash("Please login first!")
    return redirect('login')  


@app.route('/activitylog')
def activitylog():
  if session.get('user'):
    return redirect('/')
  else:
    flash("Please login first!")
    return redirect('login')  

@app.route('/dashboard/manage_user',methods=['POST'])
def dashboard_menu():
  case = request.form['userselect']
  if case == "create":
    return render_template("createuser_new.html")
  
  elif case == "delete":
    try:  
      con = mysql.connect()
      cursor = con.cursor()
      sql = "select * from user where type = %s"
      val = 'user'
      cursor.execute(sql, val)
      data = cursor.fetchall()
      
      user_lst = []
      for i in data:
        #print(i)
        #print(i[1])
        user_lst.append(i[1])
        
      
      return render_template("deleteuser_new.html", user_lst=user_lst)
    except:
      flash("unable to connect to database!")
      return redirect('/dashboard')



  
#Delete logs
@app.route('/deletelog',methods=['POST'])
def deletelog():
  if session.get('user'):
    role = session.get('role')
    #Connect to mySQL
    con = mysql.connect()
    cursor = con.cursor()
    if role == "admin" or role == "superuser":
    
      table_name = session.get('svc')
      sqlstatement = "DELETE FROM " + table_name + " where id = %s"
      val = request.form['ID']
      _val = str(val)
      cursor.execute(sqlstatement, _val)
      con.commit()  
      cursor.close()
      con.close()
      return redirect('/index/monitorlog')
    else:
      flash("You require admin role to delete logs")
      return redirect('/index/monitorlog')
      
  else:
    flash("Please login first")
    return redirect('login')


#Function to validate login info  
@app.route('/validateLogin',methods=['POST'])
def validateLogin():
  _role = request.form['loginoption']
  if _role == "admin":
    try: 
      _username = request.form['user_name']
      _password = request.form['password']
      
      username = str(_username)
      time = timenow()
      con = mysql.connect()
      cursor = con.cursor()
      sqlstatement = "select * from user where user_name = %s AND type = 'admin'"
      val = username
      cursor.execute(sqlstatement, val)
      data = cursor.fetchall()
      print(str(data[0][2]))
      print(str(data[0][3]))
      error = None

      if str(data[0][2]) == _password:
       
        topic = "account_service"

        session['user'] = data[0][1]
        session['role'] = data[0][3]
        flash('You have successfully logged in')
        return redirect('dashboard')
      else:
        error = "Invalid credentials"

     
        return render_template('monitor_loginpage.html',error = error)
    except:
      error = "No admin user found!"
      return render_template('monitor_loginpage.html',error = error)
    finally:
      cursor.close()
      con.close()
  elif _role =="user":
    try: 
      _username = request.form['user_name']
      _password = request.form['password']
      
      username = str(_username)
      time = timenow()
      con = mysql.connect()
      cursor = con.cursor()
      sqlstatement = "select * from user where user_name = %s AND type = 'user'"
      val = username
      cursor.execute(sqlstatement, val)
      data = cursor.fetchall()
      print(str(data[0][2]))
      print(str(data[0][3]))
      error = None

      if str(data[0][2]) == _password:
       
        topic = "account_service"

        session['user'] = data[0][1]
        session['role'] = data[0][3]
        flash('You have successfully logged in')
        return redirect('dashboard')
      else:
        error = "Invalid credentials"

     
        return render_template('monitor_loginpage.html',error = error)
    except:
      error = "No user found!"
      return render_template('monitor_loginpage.html',error = error)   
    finally:
      cursor.close()
      con.close()



#Cart chart
@app.route('/cart_rate_chart', methods=['GET'])
def cart_rate_chart():
  #Initialize mySQL
  con = mysql.connect()
  cursor = con.cursor()
  
  #SQL statement
  sql = "SELECT CONCAT(UPPER(SUBSTRING(item_name,1,1)),LOWER(SUBSTRING(item_name,2))) AS item_name, quantity FROM cart_service"
  cursor.execute(sql)
  row = cursor.fetchall()
  index = []
  data = {}

  for i in row:
    index.append(i)
  print(index)

  for key, value in index:
    data[key] = data.get(key, 0) + value
  print(data)

  con.commit()
  con.close()
  cursor.close()

  #Initialize 2 list and 1 dictionary
##  dlist = []
##  clist = []
##  ddict = {}

  #sort data using temp dictionary and append to list
##  for i in row:
##    temp_dict = {"cartitem": i[0]}
##    dlist.append(temp_dict)
##  print(dlist)

  #sort data from data_list into data_dict   
##  for i in dlist:
    #print(i)
##    j = i["cartitem"]
    #Check if i exist in data_dict 
##    if j not in ddict:
      #initiate value for dictionary item : 0
##      ddict[j] = 0
    #item : 0 + 1  
##    ddict[j] = ddict[j] + 1
  
##  for i in ddict:
    #Temp dict to sort items in ddict
##    temp_dict = {"Item_name" : i,
##                 "Cart_rate" : ddict.get(i)}

    #Append the temp dict to cart list  
##    clist.append(temp_dict)
##  print(clist)
  if len(row) != 0:
    notification = 0
    notification_count = count_unread()
    
    return render_template('datastreamcartrate.html', data=data, count =notification_count )
  else:
    flash("No database found!")
    return redirect('dashboard')

#Create user function
@app.route('/createuser', methods = ['POST'])
def createuser():

  if session.get('role') == "admin":
    name = request.form['user_name']
    password = request.form['password']
    _type = "user"
    try: 
      con = mysql.connect()
      cursor = con.cursor()
      
      #To check if user name exist in database 
      sqlstatement = "select * from user where user_name = %s"
      cursor.execute(sqlstatement, name)
      data = cursor.fetchone()
      
      #User name not taken 
      if data == None: 
        cursor.execute("INSERT INTO user (user_name, password, type) VALUES (%s, %s, %s)", (name, str(password), _type))
        con.commit()
        flash("User has been successfully created")  
        return redirect('dashboard')
        
      else:
        #else return error msg
        flash("User name is taken!")
        return redirect('dashboard')
      
      
    except: 
      flash("Error inserting into database!")
      return redirect('dashboard')
    finally:
      cursor.close()
      con.close()

  else:
    flash("You need admin authority to create user")
    return redirect('dashboard')
    
#Delete user function    
@app.route('/deleteuser', methods = ['POST'])
def deleteuser():
  if session.get('role') == "admin":
    name = request.form['user_name']
    #password = request.form['password']
    try: 
      con = mysql.connect()
      cursor = con.cursor()
      
      #To check if user name exist in database 
      sqlstatement = "select * from user where user_name = %s"
      cursor.execute(sqlstatement, name)
      data = cursor.fetchone()
      
      #User name not exist 
      if data == None: 
        flash("User does not exist!")  
        return redirect('dashboard')
        
      else:
        sql = "DELETE FROM user WHERE user_name = %s"
        cursor.execute(sql, name)
        con.commit()
        flash("User deleted successfully")
        return redirect('dashboard')
    except:
      flash("Error in deleting user from database!")
      return redirect('dashboard')
    finally:
      cursor.close()
      con.close()
    
  else:
    flash("You need admin authority to delete user")
    return redirect('dashboard')

@app.route('/settings_page')
def settings_page():

  f = open("threshold.txt", "r")
  threshold_data = f.read(5)
  f.close()
  return render_template("settings.html", threshold_data =threshold_data)


#Set threshold for notification
@app.route('/settings', methods = ['POST'])
def settings():
  num = request.form['threshold']

  with open("threshold.txt", "w") as myfile:
    myfile.write(num)
    
  flash("Settings updated!")
  return redirect('settings_page')
    
  #except:
    #flash("Unable to find the setting file")
    #return redirect('dashboard')
    

#Logout function
@app.route('/logout')
def logout():
  if session.get('user'):
    #Remove user from session
    session.pop('user', None)
    session.clear()
    flash("You have successfully logged out") 
    return redirect('login')
  else:
    flash("You have not log in") 
    return redirect('login')
    
#Run the program    
if __name__ == '__main__':
  app.run(port=5002, debug=True)

