from flask import Flask, render_template, url_for, request, json, redirect
from flask import session
from flaskext.mysql import MySQL
import kafka
from kafka import KafkaConsumer
from datetime import datetime
from json import loads
import time
import json
import sys
import pymysql
from flask import send_file




#mysql -u bill -p
#passpass

#create database ksql

#CREATE TABLE `kafka_topic` (
#  `id` int(11) NOT NULL AUTO_INCREMENT,
#  `message` varchar(5000) DEFAULT NULL,
#  `date` datetime DEFAULT NULL,
#  `topic` varchar(50) NOT NULL,
#  PRIMARY KEY (`id`)
#) ENGINE=InnoDB AUTO_INCREMENT=1;

#Connect to mysql
mysql = MySQL()
                     
#Connect to flask                         
app = Flask(__name__)
#secret key
app.secret_key = 'Ahgxcp1!#G'
# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'bill'
app.config['MYSQL_DATABASE_PASSWORD'] = 'passpass'
app.config['MYSQL_DATABASE_DB'] = 'ksql'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)




#Consume from kafka then send it to mySQL
#After that, retrive it from MYSQL to display it on HTML
#@app.route('/senddata')
def pull_data():
  #Topic name = request from html
  #topic = request.form['topic_name']
  #session['topic'] = topic
  
  topic = "user_logon"
  #Initialize consumer
  consumer = KafkaConsumer(topic,
   bootstrap_servers=['localhost:9092'],
   auto_offset_reset='earliest',
   #enable_auto_commit=True,
   #consumer_timeout_ms=1000,
   value_deserializer=lambda m: json.loads(m.decode('utf-8')))
 
  #initialize mysql
  con = mysql.connect()
  cursor = con.cursor()
  #Break down consumer in linesS
  while True:
# Response format is {TopicPartiton('topic1', 1): [msg1, msg2]}
    msg_pack = consumer.poll(timeout_ms=500)
    for tp, messages in msg_pack.items():
      for message in messages:
        _message = str(message)
        # message value and key are raw bytes -- decode if necessary!
        # e.g., for unicode: `message.value.decode('utf-8')`
        msg_dict = {"activity": message[0], "time" : message[1], "email":message[2]}
        cursor.execute("INSERT IGNORE INTO topic_data(email, activity, message, date, topic) VALUES (%s, %s, %s, %s, %s)", (msg_dict["email"], msg_dict["activity"], _message, msg_dict["time"], topic))
        print("Test : " + _message) 
        print ("%s:%d:%d: key=%s value=%s" % (tp.topic, tp.partition,
                                              message.offset, message.key,
                                              message.value))

      
    
    #insert into database (Without dictionary list)
    #cursor.execute("INSERT IGNORE INTO kafka_topic(message, date, topic) VALUES (%s, %s, %s)", (_message, time, topic))
    
    #Insert into database (another table)

   
  print("How to make kafka consume every 10 seconds?")
  print("How to run the functions in python every 10 seconds?")
  print("How to select and consume all kafka topics?")
  con.commit()
  con.close()  
 
  #End of first method

  
pull_data()
