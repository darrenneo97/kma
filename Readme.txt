Read me file

kafka_3.0.0 


Required Installation:

-java11jdk
-python
-command prompt
-kafka
===========================
install flask on your computer
pip3 install flask
pip3 install kafka
pip3 install flask_login
pip3 install flask-mysql
pip3 install whatever your missing. 
-------------------
Steps :
0. change directory to the folder where the python programs are located in
(cd /Desktop/E-Coms (Flask)16jan) 
1.Start kafka server 
2.Setup mysql database -> mysql -u bill -p (password: passpass)

  Start virtual environment -> python3 -m venv venv
  source venv/bin/activate 
3.Start web server -> python3 server.py
4.Start kafka pipeline -> python3 kafka_data_pipeline.py
5.Start notification server -> python3 notification_server.py
-------------------------------------------------------------
to start kafka server:
use command prompt
go to the file location kafka
type:
-> Desktop/kafka_2.13-3.0.0/bin/zookeeper-server-start.sh config/zookeeper.properties

or -> bin/zookeeper-server-start.sh config/zookeeper.properties
--------------in a seperate cmd -------------------- 
bin/kafka-server-start.sh config/server.properties

to list kafka topics:
bin/kafka-topics.sh --list --bootstrap-server localhost:9092

to create topic:
bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1  ---consumer-property group.id=my-group --topic registered_user 

to delete topic:
bin/kafka-topics.sh --bootstrap-server localhost:9092 --topic registered_user --delete

to send message to a  topic:
bin/kafka-console-producer.sh --topic registered_user --bootstrap-server localhost:9092

to consume topic:
bin/kafka-console-consumer.sh --topic user_logon --bootstrap-server localhost:9092
bin/kafka-console-consumer.sh --topic cart_topic --bootstrap-server localhost:9092

to describe / list all topics
bin/kafka-consumer-groups.sh --all-topics --bootstrap-server localhost:9092 --describe --all-groups

bin/kafka-consumer-groups.sh --topic user_logon --bootstrap-server localhost:9092 --describe --all-groups
(Note that --from-beginning can be removed)
-----------------------------------------------------

To install and setup mysel, proceed to mysql_readme.txt for detailed setup.
to start mysql database:
In command prompt, type:
mysql -u bill -p
password = passpass

use ACCOUNTS;

to view the users created:
select * from tbl_user;
-------------------------------------------------------
to start e-com web server:
go to the file location of server.py
In command prompt, type:
python3 server.py 

Enter http://127.0.0.1:5000/ in web browser
----------------
#To use virtual environment (to differentiate login session)
python3 -m venv venv
source venv/bin/activate 
------------------------------

