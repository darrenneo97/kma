from flask import Flask, render_template, url_for, request, json, redirect, jsonify
import pymysql
from datetime import time
from datetime import datetime
from datetime import timedelta
from kafka import KafkaProducer
from kafka import KafkaConsumer
from flask import flash
from flask_login import current_user, login_user

from flaskext.mysql import MySQL
from werkzeug.security import generate_password_hash, check_password_hash
import os
from flask import session

#import datetime

mysql = MySQL()
 
app = Flask(__name__)
app.secret_key = 'Ahgxcp1!#G'
#app.config['PERMANENT_SESSION_LIFETIME'] =  timedelta(minutes=10)
# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'bill'
app.config['MYSQL_DATABASE_PASSWORD'] = 'passpass'
app.config['MYSQL_DATABASE_DB'] = 'ACCOUNTS'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'

mysql.init_app(app)

#Kafka function
user = "User1"
def json_serializer(data):
  return json.dumps(data).encode("utf-8")
producer = KafkaProducer(bootstrap_servers=['10.0.2.15:9092'], value_serializer=json_serializer)
jsonproducer = KafkaProducer(bootstrap_servers =
  ['localhost:9092'],value_serializer=lambda v: json.dumps(v).encode('utf-8'))
newproducer = KafkaProducer(bootstrap_servers=['localhost:9092'],
                         value_serializer=lambda x:
                         json.dumps(x).encode('utf-8'))  
#Get time
def timenow():
  now = datetime.now()
  str_now = str(now)
  str_now = str_now[:-7]
  return str_now



def timer_now():
  now = datetime.now()
  timer = str(now)
  timer = timer[11:-9]
  timer = timer.replace(':', "")
  return timer
  
def timer_future():
  now = datetime.now() + timedelta(minutes=5)
  timer = str(now)
  timer = timer[11:-9]
  timer = timer.replace(':', "")
  return timer


@app.before_request
def before_request():
  #timeout = timedelta(minutes=2) 
  print("Time now: ", timer_now())

  
  if session.get('user'):
    x = session.get('login')
    print("Login timer: " , x)
    
    if int(timer_now()) >= int(x):
      logout()
      flash("You have been logged out after 5 minutes of inactivity")
    else:
      session['login'] = int(timer_now()) + 5
  
  
  
  #session.permanent = True
  #app.permanent_session_lifetime = timedelta(minutes=2)
  #session.modified = True


#Web page pathing
#@app.route means the hyperlink
@app.route('/', methods=['GET', 'POST'])
def index():
  server_status()
  return render_template('HomePage.html')

#Login page
@app.route('/loginpage')  
def loginpage():
  if session.get('user'):
    return redirect('/profilePage')
  else:
    return render_template('LoginPage.html')

#Home page  
@app.route('/home/')
def home():
  server_status()
  #If if session.get('user'): 
  
  # return render_template("signedinhomepage.html")
  return render_template('HomePage.html')

#Product page
@app.route('/product/')
def products():
 try:
  conn = mysql.connect()
  cursor = conn.cursor(pymysql.cursors.DictCursor)
  cursor.execute("SELECT * FROM products")
  rows = cursor.fetchall()
  return render_template('products.html', products=rows)
 except Exception as e:
  print(e)
 finally:
  cursor.close() 
  conn.close()

#Contact us page
@app.route('/contact/')
def contact():

  return render_template('ContactUsPage.html')


#Profile page  
@app.route('/profilePage')
def profilepage():
  #email = str(request.form['inputEmail'])
  if session.get('user'):
    #session['user'] = data[0][1]
    user = session.get('user')
    name = session.get('user_name')
    #conn = mysql.connect()
    #cursor = conn.cursor()
    #cursor.execute('select * from tbl_user')
    #posts = [dict(index=row[0], name=row[1],email=row[2], password=row[3] ) for row in cursor.fetchall()]       
    return render_template('profilePage.html', user=user, #posts=posts, 
    name = name )
  else:
    return render_template('error.html',error = 'Please login first')
  
#Cart page  
#SELECT count(*) FROM tbl_user WHERE username = '%s'.
@app.route('/cart/')
def cart():
  if session.get('user'):
    _user = session.get('user')
  else:
    _user = 'Not logged in'
  con = mysql.connect()
  cursor = con.cursor()
  sqlstatement= '''select * from cart where user_email = %s'''
 
  cursor.execute(sqlstatement, _user)
  #Note cursor.fetchall =/ cursor.fetchall() 
  data = cursor.fetchall()
  
  #to break down the dictionary in data
  product_dict =[]
  total_price = []
  total_qty = []
  for i in data:
    temp_dict={
      "name": i[1],
      "code": i[2],
      "quantity": i[3],
      "price": i[4],
      "total": i[3]*i[4],
      "img": str(i[2][-4:]+".jpg")}
    
    product_dict.append(temp_dict)
    #Find total qty + price
    total_price.append(temp_dict["total"])
    total_qty.append(temp_dict["quantity"])
  
  #Set them as variables
  _total_product = 0
  _total_qty = 0
  for i in total_price:    
    _total_product +=i
    
  for i in total_qty:
    _total_qty += i
    
  #print(_total_product)
  #print(_total_qty)

  return render_template('cartnew.html', data = product_dict, _total_product = _total_product, _total_qty = _total_qty)
      


#Sign up page
@app.route('/showSignUp')
def showSignUp():
  if session.get('user'):
    flash("You already have an account!")
    return redirect('/profilePage')
  else:
    return render_template('signup.html')
  

# Payment page
@app.route('/paymentpage/')
def paymentpage():
  return render_template('PaymentPage.html')
    
    #Feed back page
@app.route('/profilePage/myfeedback/')
def myfeedback():
    try:
        if session.get('user'):
            _user = session.get('user')
 
            con = mysql.connect()
            cursor = con.cursor()
            cursor.callproc('getfeedback',(_user,))
    
            feedback = cursor.fetchall()

        
            feedback_dict = []
            for i in feedback:
                feed_back_dict = {
                        'Id': i[0],
                        'Name':i[1],
                        'Email': i[2],
                        'Phone': i[3],
                        'Message': i[4],
                        'Date': i[5]}
                feedback_dict.append(feed_back_dict)
                
            for i in feedback_dict:
              print(i)
            
            return render_template('myfeedback.html', feedback_dict=feedback_dict)
        else:
            return render_template('error.html', error = 'Please login first')
    except Exception as e:
        return render_template('error.html', error = str(e))
    

@app.route('/logout')
def logout():
  #Kafka producer send message
  _email = session.get('user')
  email = str(_email)
  time = timenow()
  logoutdict = {"email" : _email , "activity": "Logout",  "time" : time, "topic": "account_service"}
  newproducer.send("kafka", logoutdict)
  #producer.send("user_logout", user + " has logged out " + time)
  
  #Remove user
  session.pop('user', None)
  session.clear()
   
  return redirect('/loginpage')
    
    
#Website Functions     
#Contact button(useless)
@app.route('/contact_button/')
def contact_button():
  time = timenow()
  producer.send("contact_button", user + " has submitted contact us " + time)
  return "Thank you for your feedback"
  
#validate user Sign up 
@app.route('/signUp',methods=['POST'])
def signUp():
# read the posted values from the UI
  _name = request.form['inputName']
  _email = request.form['inputEmail']
  _password = request.form['inputPassword']
  
  name = str(request.form['inputName'])
  email = str(request.form['inputEmail'])
  print(name, email)

# validate the received values
  if _name and _email and _password:
    conn = mysql.connect()
    cursor = conn.cursor()
    _hashed_password = generate_password_hash(_password, "sha256")
    cursor.callproc('sp_createUser',(_name,_email,_hashed_password))
    print(_hashed_password)
    print(check_password_hash(_hashed_password, _password))
    
    #print(producer.send("user_creation", name, email +  "successfully created"))
    
    
    data = cursor.fetchall()
    #if user is None or not user.check_password(form.password.data):
    #flash
    
    error = None
    print(data)
    #s = len(data) is 0
    #print(s)
    if len(data) is 0:
      conn.commit()
      time = timenow()
      #flash(notify('Account has been successfully created'))
      signup_dict = {"email" : str(_email), "activity": "Registration", "time" : time, "topic": "account_service"}
      producer.send("kafka", signup_dict)
      #return json.dumps({'message':'User created successfully !'})
      flash('Account has been successfully created! ')
      return redirect(url_for('loginpage'))
      
      
    else:
      #flash('Account has been failed to be created')
      error = "Email has been taken"
      return render_template('signup.html', error=error)
      #return json.dumps({'error':str(data[0])})
  else:
    #flash('Account has been failed to be created')
    return json.dumps({'html':'<span>Enter the required fields</span>'})
      
     
#validate Login 
@app.route('/validateLogin',methods=['POST'])
def validateLogin():
  global timer
  timer = timedelta(minutes=2)
  try:
    _username = request.form['inputEmail']
    _password = request.form['inputPassword']
    
    email = str(_username)
    time = timenow()
    #Connect to mySQL
    con = mysql.connect()
    cursor = con.cursor()
    cursor.callproc('sp_validateLogin',(_username,))
    data = cursor.fetchall()
    print(str(data[0][3]))
    error = None
    print(check_password_hash(str(data[0][3]),_password))
    if len(data) > 0:
      if check_password_hash(str(data[0][3]),_password):
        
        
        email = str(request.form['inputEmail'])	
        topic = "account_service"
        login_dict = {"activity" : "Login", "email" : email, "time": time, "topic" : topic}
        producer.send("kafka", login_dict)
        producer.send("user_logon", login_dict)
        session['user'] = data[0][2]
        session['user_name'] = data[0][1]
        session['login'] = timer_future()
        flash('You have successfully logged in')
        
        return redirect(url_for('profilepage'))
        
      else:
        error = "Invalid credentials"
        #flash('Wrong Email address or Password')
        login_dict = {"activity" : "Failed to login", "email" : email, "time": time}
        producer.send("error_login", login_dict)
        return render_template('LoginPage.html',error = error)
    else:
      error = "Invalid credentials"
      #flash('Wrong Email address or Password')
      login_dict = {"activity" : "Failed to login", "email" : email, "time": time}
      producer.send("error_login", login_dict)
      return render_template('LoginPage.html',error = error)

  except Exception as e:
    #flash('Wrong Email address or Password')
    error = "Invalid credentials"
    login_dict = {"activity" : "Failed to login", "email" : email, "time": time}
    producer.send("error_login", login_dict)
    return render_template('LoginPage.html',error = error)
    #str(e)
  finally:
    cursor.close()
    con.close()

''' # Error in code
@app.route('/signup_button/',methods=['POST','GET'])
def signup_button():
  
  _name = request.form['inputName']
  _email = request.form['inputEmail']
  if _name and _email:
    return "Account has been successfully created"
  else:
    return "Email has already been registered"
'''
  #Testing add cart to database
@app.route('/addcart',methods=['POST'])
def addcart():
  if session.get('user'):
    _user = session.get('user')
    _user = str(_user)
  else: 
    flash("Please login first")
    return redirect(url_for('loginpage'))
  try:

    
    conn = mysql.connect()
    cursor = conn.cursor()
    _code = request.form['code']
    _quantity = int(request.form['quantity'])
    
    #Get the product details from product table in database
    cursor.execute("SELECT * FROM products WHERE code=%s", _code)
    row = cursor.fetchone()

    #Get current user 

    #Temp dict to sort the item details from database into json format(dictionary)
    temp_dict = {
        "item_name" : row[2],
        "item_code" : row[1],
        "price" : row[4],
        "quantity" : _quantity,
        "total_price": _quantity * row[4]}
    print(temp_dict)
    
    time = timenow()
    
    #Set string variables activity
    activity_string = "add to cart: {}, quantity: {}".format(temp_dict["item_name"], _quantity)    
   
    kafkaArray =   {'email' : str(_user), 'activity': "Added to cart", 'item_name':temp_dict["item_name"], 'quantity':  _quantity, 'time' : time, 'topic' : 'cart_service'}

    #producer send msg to topic
    newproducer.send('kafka', kafkaArray )
   

    
    #SQL statement to check item existed in user cart
    sql_query = "Select * from cart where item_code = %s AND user_email = %s" 
    val = (_code, _user)   
    cursor.execute(sql_query, val)
    
    data = cursor.fetchall()

    #Check if item exist in sql
    #If item does not exist -> len(data)= 0
    if len(data) == 0: 
      #Proceed to insert the item into the cart database table
      cursor.execute("INSERT INTO cart(item_name, item_code, quantity, unit_price, user_email) VALUES (%s, %s, %s, %s, %s)", (temp_dict["item_name"], temp_dict["item_code"], temp_dict["quantity"], temp_dict["price"], _user))
      
    else:
      #If item already exist in cart db table
      #Update/Replace the existing field with new quantity
      sql_query = "UPDATE cart SET quantity = %s WHERE item_code = %s AND user_email = %s"
      qty = _quantity + data[0][3]
      val = (qty, _code, _user)
      cursor.execute(sql_query, val)
    
    conn.commit()
    
    #Check threshold    
    y = set_threshold()
    print("Checking threshold: "+ y)
    #Set string variables for content 
    content_string = "Over {} {}, itemcode: {} has been added to cart!".format(y, temp_dict["item_name"],temp_dict["item_code"])
    
    #Notification dict for kafka producer
    notification_dict = {"email" : str(_user), "activity": "Item added to cart exceeds threshold", "time" : time, "content": content_string }
    #Check if cart item has exceeded 100 for notification 
    #Call check_cart function
    x = check_cart(_code)
    print(check_cart(_code))
    if (x == True):
      print("SEND NOTIFICATION!")
      newproducer.send('notification', notification_dict)
    
    
    return redirect('/product')   
  except Exception as e:
    return render_template('error.html',error = str(e))
  finally:

    cursor.close()
    conn.close()

#Delete item from cart 
@app.route('/delete_item',methods=['GET', 'POST'])
def delete_item():
  
  if session.get('user'):
    _user = session.get('user')
  else:
    _user = "Not logged in"
  
  _code = request.form['code']
  
  #Initialize SQL
  conn = mysql.connect()
  #Initialize cursor in SQL
  cursor = conn.cursor()
  
  #get from db 
  sql_query = "Select * from cart WHERE item_code = %s AND user_email = %s"
  val = (str(_code), str(_user))
  
  cursor.execute(sql_query, val)
  #Fetch one sql data
  data = cursor.fetchone()
  #print(data)
  
  print("Delete: ", data)
  
  #delete from db.
  sql_delete = "DELETE FROM cart WHERE item_code = %s AND user_email = %s"
  _val = (str(_code), str(_user))
  cursor.execute(sql_delete, _val)
  conn.commit()
  conn.close()
  cursor.close()
  
  #return render_template('HomePage.html')
  return redirect(url_for('cart'))


#Empty cart 
@app.route('/empty')
def empty_cart():
  #Initialize SQL
  conn = mysql.connect()
  #Initialize cursor in SQL
  cursor = conn.cursor()
  
  try: 
    if session.get('user'):
      _user = session.get('user')
    else:
      _user = "Not logged in"
   
    #delete from db.
    sql_delete = "DELETE FROM cart WHERE user_email = %s"
    _val = (str(_user))
    cursor.execute(sql_delete, _val)
    conn.commit()
    conn.close()
    cursor.close()
  except:
    conn.rollback()
  return redirect(url_for('cart'))

#Check out function
@app.route('/checkout')
def checkout():
  #Initialize SQL
  conn = mysql.connect()
  #Initialize cursor in SQL
  cursor = conn.cursor()
  _user = session.get('user')
  sql_query = "Select * from cart WHERE user_email = %s"
  cursor.execute(sql_query, _user)
  data = cursor.fetchall()
  
  if len(data) != 0: 
    try:
      if session.get('user'):
        _user = session.get('user')
     
        #get from db 
        sql_query = "Select * from cart WHERE user_email = %s"
        val = (str(_user))
        cursor.execute(sql_query, val)
      
        #Assign data variable
        data = cursor.fetchall()
        print(data)
        
        #close mysql connect
        conn.commit()

        #to break down the dictionary in data
        product_dict =[]
        total_price = []
        total_qty = []
        for i in data:
          temp_dict={
            "name": i[1],
            "code": i[2],
            "quantity": i[3],
            "price": i[4],
            "total": i[3]*i[4]}
          product_dict.append(temp_dict)
          
          #Append total price and qty to list
          total_price.append(temp_dict["total"])
          total_qty.append(temp_dict["quantity"])
    
        #Assign variable
        _total_price = 0
        _total_qty = 0
        
        #Calculate total price of products
        #Add up every item in list
        for i in total_price:    
          _total_price +=i
      
        for i in total_qty:
          _total_qty += i
        

        conn.commit()
        conn.close()
        cursor.close()
        
      else:
        flash("Please login or register an account!")
        return redirect(url_for('loginpage'))
      
      
    except:
        conn.rollback()
    #Render html with data assigned to variables.
    return render_template('checkout.html', product_dict = product_dict, total_price = _total_price, total_qty = _total_qty)
  else: 
    flash("Your cart is empty!")
    return redirect('/cart')

#To get cart data, then insert the data into order and order details.
#Then delete cart data 
@app.route('/payment')  
def payment():
  error = 0
  try:
    #Initialize SQL
    conn = mysql.connect()
    #Initialize cursor in SQL
    cursor = conn.cursor()
    _user = session.get('user')

    #get cart data from db 
    sql_query = "Select * from cart WHERE user_email = %s"
    val = (str(_user))
    cursor.execute(sql_query, val)
    data = cursor.fetchall()
    

    #setup lists
    product_dict =[]
    total_price = []
    total_qty = []
    time = timenow()
    code = []
    
    #loop through data 
    for i in data:
    #sort data into key: value format with dictionary
      temp_dict={
        "name": i[1],
        "code": i[2],
        "quantity": i[3],
        "price": i[4],
        "total": i[3]*i[4],
        "user_email": _user,
        "Date" : time}
      product_dict.append(temp_dict)
      sql = "update products set inventory=inventory-%s where code= %s"
      val = (i[3], i[2])
      cursor.execute(sql, val)
      code.append(i[2])
      

      #Append total price and qty to list
      total_price.append(temp_dict["total"])
      total_qty.append(temp_dict["quantity"])
      #End of for loop    
    
    #check inventory stock with forloop in code list
    for i in code:
      sql_product = "select * from products where code = %s"
      
      cursor.execute(sql_product, i)
      invent = cursor.fetchall()

      #Call check_inventory function
      if check_inventory(invent[0][5], invent[0][2]) == True:
        error = error + 1
      
    #Assign variable
    _total_price = 0
    _total_qty = 0

    #Calculate total price of products
    #Add up every item in list
    for i in total_price:    
      _total_price +=i

    for i in total_qty:
      _total_qty += i
    
    #confirmed order -> insert into order database
    cursor.execute("INSERT INTO tbl_order(total_price, user_email, date) VALUES ( %s, %s, %s)", (_total_price, _user, time))
    
    
    sql_query = "Select * from tbl_order WHERE user_email = %s AND date = %s "
    val = (str(_user), time)
    cursor.execute(sql_query, val)
    data = cursor.fetchone()
    
    #Fetch data from cart again so we can transfer the data into order_details table
    sql_query = "Select * from cart WHERE user_email = %s"
    val = (str(_user))
    cursor.execute(sql_query, val)
    
    #Assign data variable
    cart_data = cursor.fetchall()
    for i in cart_data:
    #sort data into key: value format with dictionary
      temp_dict={
        "name": i[1],
        "code": i[2],
        "quantity": i[3],
        "price": i[4],
        "total": i[3]*i[4],
        "user_email": _user,
        "Date" : time}
       #Insert order details into database.
      cursor.execute("INSERT INTO order_details(orderid, item_name, item_code, quantity, unit_price, user_email) VALUES ( %s, %s, %s, %s, %s, %s)", (data[0], temp_dict["name"], temp_dict["code"], temp_dict["quantity"], temp_dict["price"], temp_dict["user_email"]))
      
    #Delete from cart as items are checked out
    del_query = "DELETE FROM cart WHERE user_email = %s"
    val = (_user)
    cursor.execute(del_query,val)   
    
    #Commit database and close
    if error == 0:
      conn.commit()
      conn.close()
      cursor.close()
      #convert data to dictionary (json like) to send to kafka topic. 
      kafka_dict={"orderid": data[0], "total_price" : data[1], "email":data[2], "activity": "Ordered", "time": time, "topic" : "order_service"}
   
      newproducer.send('kafka', kafka_dict )
    
      flash("Order has been placed successfully!")
      return redirect("/profilePage")
      
    else: 
      conn.rollback()
      conn.close()
      cursor.close()
      flash("Error: Item has insufficient stock!")
      return redirect("/profilePage")
  except:
    flash("Unable to connect to database!")
    return redirect("/profilePage")
    
    
#Order history
@app.route('/profilePage/orderhistory')
def orderhistory():
  if session.get('user'):
    user = session['user']
    name = session['user_name']
    #Initialize SQL
    conn = mysql.connect()
    #Initialize cursor in SQL
    cursor = conn.cursor()
      #get from db 
    sql_query = "Select * from tbl_order WHERE user_email = %s"
    val = (str(user))
    cursor.execute(sql_query, val)

    #Assign data variable
    data = cursor.fetchall()
    print(data)

    #close mysql connect
    conn.commit()

    
    #to break down the dictionary in data

    product_dict = []
    for i in data:
      temp_dict={
        "orderid": i[0],
        "total": i[1],
        "user_email": i[2],
        "Date": i[3]}
      product_dict.append(temp_dict)
      
    sql_query = "Select * from order_details WHERE user_email = %s"
    val = (str(user))
    cursor.execute(sql_query, val)
    order_detail = cursor.fetchall()
    
    details_list=[]
    for i in order_detail:
      temp_dict={
        "orderid": i[1],
        "Product_name": i[2],
        "Code": i[3],
        "Quantity": i[4],
        "Unit_price": i[5]
        }
      details_list.append(temp_dict)
    
    
    return render_template("orderhistory.html", details_list=details_list, product_dict = product_dict, name=name, user=user)
  else:
    flash("Please login first!")
    return redirect("loginpage")


#For notification
#Check inventory (Less than 10)
#Detect if more than set threshold has been added to cart Eg:(Over 100)
def check_cart(item_code):
  count = 0
  try:
    conn = mysql.connect()
    cursor = conn.cursor()
    email = session.get('user')
    sqlstatement = "select * from cart WHERE item_code = %s AND user_email = %s"
    
    val = (item_code, email)
    cursor.execute(sqlstatement, val)
    
    data = cursor.fetchall()
    data_list = []
    
    #Siff through table to append qty to data_list
    for i in data:
      data_list.append(i[3])

    print("Test add cart")
    print(data_list)

    count = data_list[0]
    print("Count: ", count)
    
    #set_threshold
    y = set_threshold()
    
    if count >= int(y):
      return True
    else: 
      return False
  except:
    flash("Error in connecting to database")
    return redirect('profilepage')
  
#session['user'] = data[0][2]
#18 Dec ----------------------------
#Contact us (feedback)
@app.route('/send_feedback',methods=['POST'])
def send_feedback():
  try:
    conn = mysql.connect()
    cursor = conn.cursor()
    
    if session.get('user'):
      _name = request.form['Name']
      _email = session.get('user')
      _number = request.form['Number']
      _message = request.form['Message']
      
      email = str(_email)	
      cursor.callproc('feedback',(_name,_email,_number,_message,))
      data = cursor.fetchall()
      if len(data) is 0:
        conn.commit()
        time = timenow()
        feedback_dict = {"email" : str(_email), "activity": "Sent feedback", "time" : time, "content": _message}
        
        #Kafka producer send to topic
        producer.send("notification", feedback_dict)
        flash("Thank you for your feedback!")
        return redirect('/profilePage')
      else:
        return flash("An error has occured!")
    else:
      return render_template('error.html',error = 'Please log in first')
  except Exception as e:
    return render_template('error.html',error = str(e))
  finally:
    cursor.close()
    conn.close()
    
#-----------------------------
#Search function
@app.route('/search', methods=['GET', 'POST'])
def search_result():
  search = request.form['inputSearch']
  _search = str(search)
  if session.get('user'):
    _email = str(session.get('user'))
  else:
    _email = "Not logged in"
  
  
  conn = mysql.connect()
  cursor = conn.cursor()
  cursor.execute("""select * from products WHERE name LIKE %s""", (_search,))
  conn.commit()
  data = cursor.fetchall()
  cursor.close() 
  conn.close()
  if len(data) == 0:
    #flash("Sorry, no product found")
    error = "Sorry, no product found"
    

    #Kafka producer

    return render_template('search_result.html', error = error, searchtext=_search)
  else:

    time = timenow()
    
    activity_string = "Searched keyword {} ".format(_search)
  
    search_dict = {"email" : _email, "activity" : "Searched","keyword": _search, "time" : time, "topic":"search_service"}
    #Kafka producer
    newproducer.send('kafka', search_dict)

    return render_template('search_result.html', data = data, searchtext=_search)
    
#Check current inventory
def check_inventory(qty, item):
  time= timenow()
  _email = session.get('user')
  #outofstock == False
  if qty <= 10 :
    
    _message = item + " Has less than 10 in stock!"
    low_stock = {"email" : str(_email), "activity": "Low stock", "time" : time, "content": _message}
    #Kafka producer
    newproducer.send('notification', low_stock)
  
  if qty < 0 :
    _message = item + " has no stock"
    no_stock = {"email" : str(_email), "activity": "Out of stock", "time" : time, "content": _message}
    #Kafka producer
    newproducer.send('notification', no_stock)
    #flash("The item has insufficient stock!")
    return True
    #return redirect('profilePage')
    


def set_threshold():
  fileDir = os.path.dirname(os.path.realpath('__file__'))
  filename = os.path.join(fileDir, 'Consumer_server/threshold.txt')
  #f = open("/home/linchen/Desktop/E-Coms (Flask)16jan/E-Coms (Flask)16jan/Consumer_server/threshold.txt", "r")
  f = open(filename)
  x = f.read(5)
  #y = f.readline()
  f.close()
  #print(x)
  return x
  
@app.route('/count_cart')
def count_cart():
  #try:
  if session.get('user'):
    user_email = session.get('user')
    con = mysql.connect()
    cursor = con.cursor()
  
    sql = "SELECT COUNT(*) from cart WHERE user_email = %s"
    cursor.execute(sql, user_email)
    data = cursor.fetchone()
    
    number_of_cart_item = data[0]
    
    return jsonify(number_of_cart_item = number_of_cart_item)
  
  
  #except:
    #flash('unable to connect to database')

#Test function  
def server_status():
  developer_url = 'http://127.0.0.1:5000/'
  root_url = request.url_root
  if root_url != developer_url:
  
    e_server = "Not running"
  else:
    e_server = "Running"
  e_dict = {"status" : e_server ,"topic":"server_status"}
   
  newproducer.send("server", e_dict)
  return e_server
  
  

#Test function  
#After clicking login button (Hyperlink)
#Redirect to /homepage/
@app.route('/homepage/')
#Login button
def login_button():
  print ('Login button was clicked!')
  
  #Get time and date
  time = timenow()
  
  #Kafka producer send
  #producer.send("registered_user", user + " Has login at " + time)
  #time.sleep(4)
  #Output to a file
  with open("buttonclick.txt", "a") as myfile:
  	myfile.write("User attempted to login ")
  	myfile.write(time)
  	myfile.write("\n") 

  return render_template('HomePage.html')
  


if __name__ == '__main__':

  app.run(debug=True)
