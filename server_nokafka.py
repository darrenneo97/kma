from flask import Flask, render_template, url_for, request, json, redirect
import pymysql
from datetime import datetime
#from kafka import KafkaProducer
#from kafka import KafkaConsumer
from flask import flash
from flask_login import current_user, login_user
import time
from flaskext.mysql import MySQL
from werkzeug.security import generate_password_hash, check_password_hash
	
from flask import session


mysql = MySQL()
 
app = Flask(__name__)
app.secret_key = 'why would I tell you my secret key?'
# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'bill'
app.config['MYSQL_DATABASE_PASSWORD'] = 'passpass'
app.config['MYSQL_DATABASE_DB'] = 'ACCOUNTS'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)

#Kafka function
user = "User1"
#def json_serializer(data):
#  return json.dumps(data).encode("utf-8")
#producer = KafkaProducer(bootstrap_servers=['10.0.2.15:9092'], value_serializer=json_serializer)
#jsonproducer = KafkaProducer(bootstrap_servers =
#['localhost:9092'],value_serializer=lambda v: json.dumps(v).encode('utf-8'))
  
#Get time
def timenow():
  now = datetime.now()
  str_now = str(now)
  str_now = str_now[:-6]
  return str_now

#Web page pathing
#@app.route means the hyperlink
@app.route('/', methods=['GET', 'POST'])
def index():
  return render_template('HomePage.html')

#Login page
@app.route('/loginpage')  
def loginpage():
  if session.get('user'):
    return redirect('/profilePage')
  else:
    return render_template('LoginPage.html')

#Home page  
@app.route('/home/')
def home():
  #If if session.get('user'): 
  # return render_template("signedinhomepage.html")
  return render_template('HomePage.html')

#Product page
@app.route('/product/')
def products():
 try:
  conn = mysql.connect()
  cursor = conn.cursor(pymysql.cursors.DictCursor)
  cursor.execute("SELECT * FROM products")
  rows = cursor.fetchall()
  return render_template('products.html', products=rows)
 except Exception as e:
  print(e)
 finally:
  cursor.close() 
  conn.close()

#Contact us page
@app.route('/contact/')
def contact():
  time = timenow()
  #producer.send("contact", user + " has clicked on contact us " + time)
  return render_template('ContactUsPage.html')


#Profile page  
@app.route('/profilePage')
def profilepage():
  #email = str(request.form['inputEmail'])
  if session.get('user'):
    #session['user'] = data[0][1]
    user = session['user']
    name = session['user_name']
    #conn = mysql.connect()
    #cursor = conn.cursor()
    #cursor.execute('select * from tbl_user')
    #posts = [dict(index=row[0], name=row[1],email=row[2], password=row[3] ) for row in cursor.fetchall()]       
    return render_template('profilePage.html', user=user, #posts=posts, 
    name = name )
  else:
    return render_template('error.html',error = 'Please login first')
  
#Cart page  
@app.route('/cart/')
def cart():
  return render_template('cart.html')
      


#Sign up
@app.route('/showSignUp')
def showSignUp():
  if session.get('user'):
    return redirect('/profilePage')
  else:
    return render_template('signup.html')
  


@app.route('/paymentpage/')
def paymentpage():
  return render_template('PaymentPage.html')
    
@app.route('/logout')
def logout():
  session.pop('user', None)
  return redirect('/loginpage')
    
    
#Website Functions     
#Contact button
@app.route('/contact_button/')
def contact_button():
  time = timenow()
  #producer.send("contact_button", user + " has submitted contact us " + time)
  return "Thank you for your feedback"
  
#validate user Sign up 
@app.route('/signUp',methods=['POST'])
def signUp():
# read the posted values from the UI
  _name = request.form['inputName']
  _email = request.form['inputEmail']
  _password = request.form['inputPassword']
  
  name = str(request.form['inputName'])
  email = str(request.form['inputEmail'])
  print(name, email)

# validate the received values
  if _name and _email and _password:
    conn = mysql.connect()
    cursor = conn.cursor()
    _hashed_password = generate_password_hash(_password)
    cursor.callproc('sp_createUser',(_name,_email,_hashed_password))
    print(_hashed_password)
    print(check_password_hash(_hashed_password, _password))
    
    #print(producer.send("user_creation", name, email +  "successfully created"))
    
    
    data = cursor.fetchall()
    #if user is None or not user.check_password(form.password.data):
    #flash
    
    error = None
    print(data)
    #s = len(data) is 0
    #print(s)
    if len(data) is 0:
      conn.commit()
      time = timenow()
      #flash(notify('Account has been successfully created'))
      #producer.send("user_creation", "New user" + email +  " has been successfully created " + time)
      #return json.dumps({'message':'User created successfully !'})
      flash('Account has been successfully created! ')
      return redirect('/loginpage')
      
    else:
      #flash('Account has been failed to be created')
      error = "Please check your input"
      return render_template('signup.html', error=error)
      #return json.dumps({'error':str(data[0])})
  else:
    #flash('Account has been failed to be created')
    return json.dumps({'html':'<span>Enter the required fields</span>'})
      
     
#validate Login 
@app.route('/validateLogin',methods=['POST'])
def validateLogin():
  try:
    _username = request.form['inputEmail']
    _password = request.form['inputPassword']
    
    
    #Connect to mySQL
    con = mysql.connect()
    cursor = con.cursor()
    cursor.callproc('sp_validateLogin',(_username,))
    data = cursor.fetchall()
    print(str(data[0][3]))
    error = None
    print(check_password_hash(str(data[0][3]),_password))
    if len(data) > 0:
      if check_password_hash(str(data[0][3]),_password):
        time = timenow()
        email = str(request.form['inputEmail'])	
        #producer.send("user_logon", "User " + email+  " has logged on " + time)
        session['user'] = data[0][2]
        session['user_name'] = data[0][1]
        flash('You have successfully logged in')
        return redirect(url_for('profilepage'))
      else:
        error = "Invalid credentials"
        #flash('Wrong Email address or Password')
        return render_template('LoginPage.html',error = error)
    else:
      error = "Invalid credentials"
      #flash('Wrong Email address or Password')
      return render_template('LoginPage.html',error = error)

  except Exception as e:
    #flash('Wrong Email address or Password')
    error = "Invalid credentials"
    return render_template('LoginPage.html',error = error)
    #str(e)
  finally:
    cursor.close()
    con.close()

''' # Error in code
@app.route('/signup_button/',methods=['POST','GET'])
def signup_button():
  
  _name = request.form['inputName']
  _email = request.form['inputEmail']
  if _name and _email:
    return "Account has been successfully created"
  else:
    return "Email has already been registered"
'''
  #Testing add cart to database
@app.route('/addcart',methods=['POST'])
def addcart():
  try:
    if session.get('user'):
      _quantity = int(request.form['quantity'])
      _code = request.form['code']
      _user = session.get('user')
#if _quantity and _code and request.method == 'POST':

      conn = mysql.connect()
      cursor = conn.cursor()
      cursor.callproc('sp_addcart',(_quantity,_code,_user))
      data = cursor.fetchall()

      if len(data) is 0:
        conn.commit()
        return redirect('/home/')
      else:
        return render_template('error.html',error = 'An error occurred!')

    else:
      return render_template('error.html',error = 'Unauthorized Access')
  except Exception as e:
    return render_template('error.html',error = str(e))
  finally:
    cursor.close()
    conn.close()

#Get cart from database
@app.route('/getcart')
def getcart():
  try:
    if session.get('user'):
      _user = session.get('user')

      con = mysql.connect()
      cursor = con.cursor()
      cursor.callproc('sp_GetcartByUser',(_user,))
#cursor.execute("SELECT * FROM products WHERE code=%s", _code)
      wishes = cursor.fetchall()

      wishes_dict = []
      for cart in carts:
        cart_dict = {
          'Id': cart[0],
          'name': cart[1],
          'product_code': wish[2],
          'Date': wish[4]}
        carts_dict.append(cart_dict)

      return json.dumps(carts_dict)
    else:
      return render_template('error.html', error = 'Unauthorized Access')
  except Exception as e:
    return render_template('error.html', error = str(e))
  
#Add to cart function
@app.route('/add', methods=['POST'])
def add_product_to_cart():
 cursor = None
 try:
  _quantity = int(request.form['quantity'])
  _code = request.form['code']
  # validate the received values
  if _quantity and _code and request.method == 'POST':
   conn = mysql.connect()
   cursor = conn.cursor(pymysql.cursors.DictCursor)
   cursor.execute("SELECT * FROM products WHERE code=%s", _code)
   row = cursor.fetchone()
    
   itemArray = { row['code'] : {'name' : row['name'], 'code' : row['code'], 'quantity' : _quantity, 'price' : row['price'], 'image' : row['image'], 'total_price': _quantity * row['price']}}
   #kafkaArray = {  'name' : row['name'], 'code' : row['code'], 'quantity' : _quantity, 'price' : row['price'], 'total_price': _quantity * row['price']}
   #Kafka producer
   time = timenow()
   #producer.send('cart_topic', 'An item has been added to cart'+ time)
   #jsonproducer.send('cart_topic', kafkaArray )
   
   all_total_price = 0
   all_total_quantity = 0
    
   session.modified = True
   if 'cart_item' in session:
    if row['code'] in session['cart_item']:
     for key, value in session['cart_item'].items():
      if row['code'] == key:
       old_quantity = session['cart_item'][key]['quantity']
       total_quantity = old_quantity + _quantity
       session['cart_item'][key]['quantity'] = total_quantity
       session['cart_item'][key]['total_price'] = total_quantity * row['price']
    else:
     session['cart_item'] = array_merge(session['cart_item'], itemArray)
 
    for key, value in session['cart_item'].items():
     individual_quantity = int(session['cart_item'][key]['quantity'])
     individual_price = float(session['cart_item'][key]['total_price'])
     all_total_quantity = all_total_quantity + individual_quantity
     all_total_price = all_total_price + individual_price
   else:
    session['cart_item'] = itemArray
    all_total_quantity = all_total_quantity + _quantity
    all_total_price = all_total_price + _quantity * row['price']
    
   session['all_total_quantity'] = all_total_quantity
   session['all_total_price'] = all_total_price
    
   return redirect(url_for('.products'))
  else:
   return 'Error while adding item to cart'
 except Exception as e:
  print(e)
 finally:
  cursor.close() 
  conn.close()


 #Empty cart
@app.route('/empty')
def empty_cart():
 try:
  session.clear()
  return redirect(url_for('.products'))
 except Exception as e:
  print(e)

#Delete item from cart 
@app.route('/delete/<string:code>')
def delete_product(code):
 try:
  all_total_price = 0
  all_total_quantity = 0
  session.modified = True
   
  for item in session['cart_item'].items():
   if item[0] == code:    
    session['cart_item'].pop(item[0], None)
    if 'cart_item' in session:
     for key, value in session['cart_item'].items():
      individual_quantity = int(session['cart_item'][key]['quantity'])
      individual_price = float(session['cart_item'][key]['total_price'])
      all_total_quantity = all_total_quantity + individual_quantity
      all_total_price = all_total_price + individual_price
    break
   
  if all_total_quantity == 0:
   session.clear()
  else:
   session['all_total_quantity'] = all_total_quantity
   session['all_total_price'] = all_total_price
   
  return redirect(url_for('.products'))
 except Exception as e:
  print(e)  
  
#Merge cart  
def array_merge( first_array , second_array ):
 if isinstance( first_array , list ) and isinstance( second_array , list ):
  return first_array + second_array
 elif isinstance( first_array , dict ) and isinstance( second_array , dict ):
  return dict( list( first_array.items() ) + list( second_array.items() ) )
 elif isinstance( first_array , set ) and isinstance( second_array , set ):
  return first_array.union( second_array )
 return False  
  
  
#Test function  
#After clicking login button (Hyperlink)
#Redirect to /homepage/
@app.route('/homepage/')
#Login button
def login_button():
  print ('Login button was clicked!')
  
  #Get time and date
  time = timenow()
  
  #Kafka producer send
  #producer.send("registered_user", user + " Has login at " + time)
  #time.sleep(4)
  #Output to a file
  with open("buttonclick.txt", "a") as myfile:
  	myfile.write("User attempted to login ")
  	myfile.write(time)
  	myfile.write("\n") 

  return render_template('HomePage.html')
  

  
if __name__ == '__main__':
  app.run(debug=True)
