from flask import Flask, render_template, url_for, request, json, redirect
from flask import session
from flaskext.mysql import MySQL
import kafka
from kafka import KafkaConsumer
from datetime import datetime
from json import loads
import time
import json
import sys
import pymysql
from flask import send_file


#Connect to mysql
mysql = MySQL()
                     
#Connect to flask                         
app = Flask(__name__)
#secret key
app.secret_key = 'Ahgxcp1!#G'
# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'bill'
app.config['MYSQL_DATABASE_PASSWORD'] = 'passpass'
app.config['MYSQL_DATABASE_DB'] = 'ksql'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)

#Consume Notifications from kafka producer
def pull_data():
  #Topic name = request from html
  #topic = request.form['topic_name']
  #session['topic'] = topic
  
  topic = "notification"
  #Initialize consumer
  consumer = KafkaConsumer(topic,
     bootstrap_servers=['localhost:9092'],
     auto_offset_reset='latest',
     enable_auto_commit=True,
     consumer_timeout_ms=1000,
     value_deserializer=lambda m: json.loads(m.decode('utf-8')))
  
  #initialize mysql
  con = mysql.connect()
  cursor = con.cursor()
 

  #Break down consumer in lines
  for i in consumer:
    message = i.value
    _message = str(message)
    msg_dict = {"activity": message["activity"], "time" : message["time"], "email": message["email"]}
    
    
    print("Notification detected: " + _message) 
    
    #Send notification data to mysql DB
    cursor.execute("INSERT IGNORE INTO notification(email, activity, content, date, time, message) VALUES (%s, %s, %s, %s, %s, %s)", (msg_dict["email"], msg_dict["activity"], message["content"], msg_dict["time"][:10], msg_dict["time"][11:], _message))

      

    

    #insert into database (Without dictionary list)
    #cursor.execute("INSERT IGNORE INTO kafka_topic(message, date, topic) VALUES (%s, %s, %s)", (_message, time, topic))
    

  con.commit()
  con.close()  
 
      
def delete_kafka_topic():
  call(["/Desktop/kafka_2.13-3.0.0/bin/kafka-topics/sh", "--zookeeper", "zookeeper-1:2181", "--delete", "--topic", notification])
  
while True:  
  pull_data()
  #delete_kafka_topic()
